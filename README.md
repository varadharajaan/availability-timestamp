# Terraform/AWS Auto Scaling Group with ELB 

This module provisions the resources necessary to run a (docker) application in an Auto Scaling Group (ASG) of EC2 instances. This module should be production ready. This module is available in the [terraform registry](https://registry.terraform.io/modules/EconomistDigitalSolutions/asg/aws/)

## Table of contents

- [Terraform/AWS Auto Scaling Module](#terraformaws-auto-scaling-module)
  - [Usage](#usage)
  - [Bugs/Known Issues](#bugsknown-issues)
  - [Implementation details](#implementation-details)
  - [Illustration](#illustration)
  - [Output](#output)
  - [Multi-account Deployment](#Multi-account Deployment)

## Usage

To use this module, it is advised to carefully follow these instructions: 

1) Create a repository for your project
   ```
   mkdir my-app; cd my-app
   ```
   

2) Provision the infrastructure (and deploy the app on instance start):
    ```
    terraform init; terraform apply --auto-approve
    ```

4) To tear down the infrastructure, run
    ```
    terraform init; terraform destroy --auto-approve
    ```
<hr />


## Bugs/Known Issues

* when changing region - can not migrate load-balancer
  * this issue is found tracked on:
    * stackoverflow [https://stackoverflow.com/questions/54650350/aws-load-balancer-change-region-with-terraform](https://stackoverflow.com/questions/54650350/aws-load-balancer-change-region-with-terraform)
    * Terraform [https://github.com/terraform-providers/terraform-provider-aws/issues/7517](https://github.com/terraform-providers/terraform-provider-aws/issues/7517)

* Multiple concurrent deployments may lead to issues with the terraform state lock, creating a race condition and possibly stale deployments;
* New deployments follow Blue/Green approach. A new Auto Scaling Group is created on every deployments. The teardown of the old ASG takes a long time (approx. 7 minutes).

<hr />


## Implementation details

Below is a simplified and complete list of the AWS resources deployed.


The **complete** stack:
  * Compute
    * EC2 instances
  * Scale
    * Auto Scaling Group
    * ASG attachment (to load balancer)
    * EC2 launch configuration
  * Load Balancers
    * Load balancer
    * LB Target Group
    * LB listener
  * Network
    * Virtual Private Cloud
    * Subnets (Public)
    * Internet Gateway
    * Routing
      * route tables
      * route table associations (to subnet)
    * Security Groups  
      * for instances
      * for load balancer
  * Domain (optional)
    * Route53 record

<hr/>

## Illustration

![Simplified illustration of the deployed stack:](img/elb.png)

<hr/>

## Output

| Name | Description |
|------|-------------|
| domain-load-balancer | The public domain of the load-balancer |

### Multi-account Deployment

* Master branch pipeline will get deployed to production
* Develop  branch pipeline will get deployed to Staging 
* Any other branch piepleine will get deployed to sandbox
* Destory Job is able to trigger when Variable ENVIORNMENT and DESTORY is provided

