terraform {
  backend "s3" {
    bucket = "tf-available"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}
