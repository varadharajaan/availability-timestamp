
region                                  = "eu-west-2"
name                                    = "terraform"

# VPC Variables
vpc_env                                 = "dev"

vpc_enable_nat_gateway                  = false
vpc_single_nat_gateway                  = false
vpc_one_nat_gateway_per_az              = false

max_private_subnet_count                = 3
max_public_subnet_count                 = 3
private_subnet_start                    = 11
public_subnet_start                     = 1

#App
app_port                                = "8080"
app_instance_type                       = "t2.micro"
app_autoscale_min_size                  = "2"
app_autoscale_max_size                  = "3"
app_elb_health_check_interval           = "20"
app_elb_healthy_threshold               = "2"
app_elb_unhealthy_threshold             = "2"
app_elb_health_check_timeout            = "5"
app_key_pair_name                       = "terraform"
