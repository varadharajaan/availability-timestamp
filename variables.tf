variable "region" {
  default = "eu-west-2"
}

variable "name" {
  default = "terraform"
}

variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCRQYYw42SeWm4SvTt4iipyjozdpaR5FlNeyo3oWYXF2W2Uur+XXJssFhxD8xf0NFKRtrK3wVZPkP/7k6+eRufC9Lq6VZvImlCTFJmEy+uHnA+vlkoXbUGk2zr7Cpct7udpZZxSivt+7lQ4avhBCQE/hw1qZxdGyZJY1Z1F3LGHCP55a+h5XxtaZR0eJQmejWnG9wq++iywdeOH2tCeOsnyNw1bjhYfydEDK7OAh/sZYsroxGpk/0SNFyscy/x2zEBrveDppE6QlH9pffx50mV00OgefHx2wpa95jwG7RWKvUkwqqMx1bEbg7tZ76PfNTM/rRg+EV9d1NMKQ3R5S1zT"
}

# VPC Variables
variable "vpc_env" {
  description = "VPC environment"
  default     = "dev"
}

variable "vpc_enable_nat_gateway" {
  description = "Should be true if you want to provision NAT Gateways for each of your private networks"
  default     = false
}

variable "vpc_single_nat_gateway" {
  description = "Should be true if you want to provision a single shared NAT Gateway across all of your private networks"
  default     = false
}

variable "vpc_one_nat_gateway_per_az" {
  description = "Should be true if you want only one NAT Gateway per availability zone. Requires `var.azs` to be set, and the number of `public_subnets` created to be greater than or equal to the number of availability zones specified in `var.azs`."
  default     = false
}


# App
variable "app_port" {
  description = "The port on which the application listens for connections"
  default     = 8080
}

variable "app_instance_type" {
  description = "The EC2 instance type for the application servers"
  default     = "t2.micro"
}

variable "app_autoscale_min_size" {
  description = "The fewest amount of EC2 instances to start"
  default     = 3
}

variable "app_autoscale_max_size" {
  description = "The largest amount of EC2 instances to start"
  default     = 6
}

variable "app_elb_health_check_interval" {
  description = "Duration between health checks"
  default     = 20
}

variable "app_elb_healthy_threshold" {
  description = "Number of checks before an instance is declared healthy"
  default     = 2
}

variable "app_elb_unhealthy_threshold" {
  description = "Number of checks before an instance is declared unhealthy"
  default     = 2
}

variable "app_elb_health_check_timeout" {
  description = "Interval between checks"
  default     = 5
}


# Web

variable "web_port" {
  description = "The port on which the web servers listen for connections"
  default     = 80
}

variable "web_instance_type" {
  description = "The EC2 instance type for the web servers"
  default     = "t2.micro"
}

variable "web_autoscale_min_size" {
  description = "The fewest amount of EC2 instances to start"
  default     = 2
}

variable "web_autoscale_max_size" {
  description = "The largest amount of EC2 instances to start"
  default     = 3
}


variable "environment" {
  type = string
  default = "production"
  description = "Options: development, qa, staging, production"
}

variable "cidr_ab" {
  type = map
  default = {
    development     = "10.7"
    qa              = "10.7"
    staging         = "10.7"
    production      = "10.7"
  }
}

variable "max_private_subnet_count" {
  default = 3
}

variable "max_public_subnet_count" {
  default = 3
}

variable "private_subnet_start" {
  default = 11
}

variable "public_subnet_start" {
  default = 1
}
