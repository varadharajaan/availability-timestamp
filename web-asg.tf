resource "aws_security_group" "web" {
  name = format("%s-web-sg", var.name)

  vpc_id = module.vpc.vpc_id

  ingress {
    from_port = var.app_port
    to_port   = var.app_port
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 0
    to_port   = 64435
    protocol  = "tcp"

    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = "0"
    to_port     = "65535"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Group = var.name
    Name  = "web-sg"
  }
}

resource "aws_key_pair" "mykeypair" {
  key_name   = "mykeypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJVGHssIRhqML0UGQY7q8pQmU9NmFj4AaFQ/7AzeFWeWuvbtBC8vUG3+zaOZmO62RFGACBSlk+TEmvdqyD0VPh9brBTDEff8XNIJVt+okWXjIsORlGSt7X9ZlS4No5j59ZxO8ng+c1/0CuscEhlg1T7s8sZhVAq9EUWEt1VsFWb8GjMi5avAHHyZzE6SdlASc+u03uGPNuLVcRnHYuvF97PK5jgtTpvmqq1djZr+N/nMitsvVQZYUrMQ3TJ59a6ZvJ14kClbyqUtpOqTKUZMZvlS0sec2N+KEnqkxexdChs0NcN0IXID0X6AWhe529iTP0PQ3NViSxo7UBJvkt7avz"
  lifecycle {
    ignore_changes = [public_key]
  }
}

resource "aws_launch_configuration" "web" {
  image_id        = data.aws_ami.amazon_linux.id
  instance_type   = var.web_instance_type
  security_groups = [aws_security_group.web.id]

  #TODO REMOVE
  key_name    = aws_key_pair.mykeypair.key_name
  name_prefix = "${var.name}-web-vm-"

  user_data =  <<-EOF
              #!/bin/bash
              yum install -y java-1.8.0-openjdk-devel wget git
              export JAVA_HOME=/etc/alternatives/java_sdk_1.8.0
              wget http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-apache-maven.repo -O /etc/yum.repos.d/epel-apache-maven.repo
              sed -i s/\$releasever/6/g /etc/yum.repos.d/epel-apache-maven.repo
              yum install -y apache-maven
              yum install -y docker
              service docker start
              docker pull varadharajaan/timestamp:latest
              docker run -p 8080:8080 varadharajaan/timestamp:latest
              EOF

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "web" {
  launch_configuration = aws_launch_configuration.web.id


  vpc_zone_identifier = module.vpc.public_subnets


  load_balancers = [module.elb_web.this_elb_name]
  health_check_type = "EC2"

  min_size = var.web_autoscale_min_size
  max_size = var.web_autoscale_max_size

  tags = [
  {
    key = "Group"
    value = var.name
    Name = "web-asg-${var.name}"
    propagate_at_launch = true
  }
]
}
